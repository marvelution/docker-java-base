FROM alpine:3.8
MAINTAINER Marvelution <info@marvelution.com>

ARG GLIBC_VERSION=2.28-r0
ARG JAVA_VERSION_MAJOR
ARG JAVA_VERSION_MINOR
ARG JAVA_VERSION_BUILD
ARG JAVA_DOWNLOAD_HASH

# Update
RUN apk update \
 && apk upgrade \
 && apk add --no-cache tar openssl curl ca-certificates \
 && curl -sSL https://alpine-pkgs.sgerrand.com/sgerrand.rsa.pub -o /etc/apk/keys/sgerrand.rsa.pub \
 && for pkg in glibc-${GLIBC_VERSION} glibc-bin-${GLIBC_VERSION} glibc-i18n-${GLIBC_VERSION}; do curl -sSL https://github.com/andyshinn/alpine-pkg-glibc/releases/download/${GLIBC_VERSION}/${pkg}.apk -o /tmp/${pkg}.apk; done \
 && apk add /tmp/*.apk \
 && rm -v /tmp/*.apk \
 && rm -rf /var/cache/apk/* \
 && ( /usr/glibc-compat/bin/localedef --force --inputfile POSIX --charmap UTF-8 C.UTF-8 || true ) \
 && echo "export LANG=C.UTF-8" > /etc/profile.d/locale.sh \
 && /usr/glibc-compat/sbin/ldconfig /lib /usr/glibc-compat/lib \
 && echo 'hosts: files mdns4_minimal [NOTFOUND=return] dns mdns4' >> /etc/nsswitch.conf

# Download and unarchive Java
RUN mkdir /opt \
 && curl -jksSLH "Cookie: oraclelicense=accept-securebackup-cookie" \
	     https://download.oracle.com/otn-pub/java/jdk/${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-b${JAVA_VERSION_BUILD}/${JAVA_DOWNLOAD_HASH}/jdk-${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}-linux-x64.tar.gz \
	     | tar -xzf - -C /opt \
 && ln -s /opt/jdk1.${JAVA_VERSION_MAJOR}.0_${JAVA_VERSION_MINOR} /opt/jdk \
 && rm -rf /opt/jdk/*src.zip \
		   /opt/jdk/lib/missioncontrol \
		   /opt/jdk/lib/visualvm \
		   /opt/jdk/lib/*javafx* \
		   /opt/jdk/jre/lib/plugin.jar \
		   /opt/jdk/jre/lib/ext/jfxrt.jar \
		   /opt/jdk/jre/bin/javaws \
		   /opt/jdk/jre/lib/javaws.jar \
		   /opt/jdk/jre/lib/desktop \
		   /opt/jdk/jre/plugin \
		   /opt/jdk/jre/lib/deploy* \
		   /opt/jdk/jre/lib/*javafx* \
		   /opt/jdk/jre/lib/*jfx* \
		   /opt/jdk/jre/lib/amd64/libdecora_sse.so \
		   /opt/jdk/jre/lib/amd64/libprism_*.so \
		   /opt/jdk/jre/lib/amd64/libfxplugins.so \
		   /opt/jdk/jre/lib/amd64/libglass.so \
		   /opt/jdk/jre/lib/amd64/libgstreamer-lite.so \
		   /opt/jdk/jre/lib/amd64/libjavafx*.so \
		   /opt/jdk/jre/lib/amd64/libjfx*.so \
 && /opt/jdk/bin/java -version

# Set environment
ENV JAVA_HOME /opt/jdk
ENV PATH ${JAVA_HOME}/bin:${PATH}

COPY SSLPoke.class /root/
