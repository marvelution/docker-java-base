#!/bin/sh
set -o errexit

repo=072543529597.dkr.ecr.eu-west-1.amazonaws.com

JAVA_VERSION_MAJOR=8
JAVA_VERSION_MINOR=192

tag=${JAVA_VERSION_MAJOR}u${JAVA_VERSION_MINOR}

echo "Building java-base:${tag} without using the cache"
docker build -t marvelution/java-base \
  --build-arg JAVA_VERSION_MAJOR=${JAVA_VERSION_MAJOR} \
  --build-arg JAVA_VERSION_MINOR=${JAVA_VERSION_MINOR} \
	--build-arg JAVA_VERSION_BUILD=12 \
	--build-arg JAVA_DOWNLOAD_HASH=750e1c8617c5452694857ad95c3ee230 \
  --no-cache .

docker tag marvelution/java-base:latest ${repo}/marvelution/java-base:${tag}

echo "Pushing to AWS ECR"
docker push ${repo}/marvelution/java-base:${tag}
